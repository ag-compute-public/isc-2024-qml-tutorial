{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "597e4439",
   "metadata": {},
   "source": [
    "# 3) Quantum Classification"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "d3b8456a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import seaborn as sns\n",
    "import time\n",
    "\n",
    "from matplotlib import pyplot as plt\n",
    "from IPython.display import clear_output\n",
    "from multiprocessing import Pool\n",
    "\n",
    "from sklearn.svm import SVC\n",
    "from sklearn.decomposition import PCA\n",
    "from sklearn.cluster import SpectralClustering\n",
    "from sklearn.datasets import load_iris, make_blobs\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.metrics import normalized_mutual_info_score\n",
    "from sklearn.preprocessing import MinMaxScaler, StandardScaler\n",
    "\n",
    "from qiskit import transpile\n",
    "from qiskit_aer import AerSimulator\n",
    "from qiskit_algorithms.state_fidelities import ComputeUncompute\n",
    "from qiskit_algorithms.optimizers import COBYLA, SPSA, SLSQP\n",
    "from qiskit_machine_learning.kernels import FidelityQuantumKernel\n",
    "from qiskit_machine_learning.algorithms.classifiers import VQC\n",
    "from qiskit_machine_learning.algorithms import QSVC\n",
    "from qiskit_ibm_runtime.fake_provider import FakeBoeblingen\n",
    "from qiskit.circuit.library import ZFeatureMap, ZZFeatureMap, RealAmplitudes, EfficientSU2\n",
    "from qiskit.visualization import plot_histogram\n",
    "from qiskit.primitives import Sampler"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7883b4cd",
   "metadata": {},
   "source": [
    "## Support Vector Classifiers"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d242a738",
   "metadata": {},
   "source": [
    "We would like to compare the capabilities of classifying data using classical algorithms, compared to quantum algorithms. For our classical algorithm, we implement a __Support Vector Classifier (SVC)__. Recall, a SVC is a supervised learning classifier that finds a hyperplane using the kernel trick that correctly separates two classes with a maximum margin."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54355d8a",
   "metadata": {},
   "outputs": [],
   "source": [
    "iris = load_iris()     # Dataset\n",
    "X = iris.data          # 4 features x 150 samples (50 per 1 of 3 labels)\n",
    "y = iris.target        # 3 labels\n",
    "\n",
    "# Scale feature data to [0,1]\n",
    "X = MinMaxScaler().fit_transform(X)\n",
    "\n",
    "# Partition training and testing data\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=123)\n",
    "\n",
    "# Create classifier and fit training data\n",
    "svc = SVC(kernel=\"rbf\", random_state=123)\n",
    "svc.fit(X_train, y_train)\n",
    "\n",
    "# Test performance of classical model\n",
    "train_score_classical = svc.score(X_train, y_train)\n",
    "test_score_classical = svc.score(X_test, y_test)\n",
    "print(f\"Classical SVC on the training dataset: {train_score_classical:.3f}\")\n",
    "print(f\"Classical SVC on the test dataset:     {test_score_classical:.3f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "125132d5",
   "metadata": {},
   "source": [
    "The SVC performs very well on the data!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08bf5b76",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualize the data\n",
    "plt.clf()\n",
    "plt.rcParams[\"figure.figsize\"] = (6, 6)\n",
    "sns.scatterplot(x=X[:, 0], y=X[:, 1], hue=y, palette=\"tab10\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3462924e",
   "metadata": {},
   "source": [
    "# Quantum SVC"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2588bfc",
   "metadata": {},
   "source": [
    "The __QSVC__ class within Qiskit serves as an extension to SVC from its Scikit-learn class. In order to use QSVC, we first need a kernel to equip it with. To define the kernel we require a feature map and a fidelity. The __fidelity__ would simply give an assessment of two quantum states when overlapping them. We require a __Sampler__ primitive for the fidelity, which supplies distributions for the circuit."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "610e07fc",
   "metadata": {},
   "source": [
    "__Quantum Feature Maps__ are parameterized quantum circuits that encode the data.  The __ZZFeatureMap__ encodes interactions in the data according to the connectivity graph and the classical data map. The parameters we need to specify are how many features the data has, and how many repetitions we would like to apply. More repetitions lead to more entanglement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "07ec35f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "feature_map = ZZFeatureMap(feature_dimension=4, reps=2, entanglement=\"linear\")\n",
    "sampler = Sampler()     # ComputeUncompute would define Sampler default; here for demonstration\n",
    "fidelity = ComputeUncompute(sampler=sampler)     # combine circuit for comparison circuit\n",
    "kernel = FidelityQuantumKernel(fidelity=fidelity, feature_map=feature_map) # classification kernel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "557233cc",
   "metadata": {},
   "source": [
    "We can now prepare and fit the classifier and obtain a score."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b5594ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Quantum SVC with quantum kernel\n",
    "qsvc = QSVC(quantum_kernel=kernel)\n",
    "qsvc.fit(X_train, y_train)\n",
    "qsvc_score = qsvc.score(X_test, y_test)\n",
    "\n",
    "print(f\"QSVC score: {qsvc_score}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf66c412",
   "metadata": {},
   "source": [
    "How does this compare with implementing a classical SVC? And how would the quantum kernel act on the classical SVC if we were to evaluate it for use with SVC?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "05c7992d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Classical SVC with evaluated quantum kernel\n",
    "matrix_train = kernel.evaluate(x_vec=X_train)\n",
    "matrix_test = kernel.evaluate(x_vec=X_test, y_vec=X_train)\n",
    "\n",
    "prec_svc = SVC(kernel=\"precomputed\", random_state=123)\n",
    "prec_svc.fit(matrix_train, y_train)\n",
    "prec_score = prec_svc.score(matrix_test, y_test)\n",
    "\n",
    "# Classical SVC with classical linear kernel\n",
    "lin_svc = SVC(kernel=\"linear\", random_state=123)\n",
    "lin_svc.fit(X_train, y_train)\n",
    "score = lin_svc.score(X_test, y_test)\n",
    "\n",
    "# Print scores\n",
    "print(f\"Precomputed kernel SVC score: {prec_score}\")\n",
    "print(f\"Linear SVC score: {score}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "57f2508b",
   "metadata": {},
   "source": [
    "Notice two things: <br>\n",
    "(1) The QSVC score is the same as the precomputed SVC score, as it should be, since QSVC simply inherits SVC and they used the same kernel. <br>\n",
    "(2) The linear SVC actually outperformed the QSVC. This is often the case currently as quantum kernels are still to improve a lot. For a large dataset, a quantum kernel might be evaluated much faster with reasonable accuracy, but for superior accuracy, classical kernels are still more accurate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94a5661f",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Visualize the kernel matrices\n",
    "fig, axs = plt.subplots(1, 2, figsize=(10, 5))\n",
    "axs[0].imshow(\n",
    "    np.asmatrix(matrix_train), interpolation=\"nearest\", origin=\"upper\", cmap=\"Blues\"\n",
    ")\n",
    "axs[0].set_title(\"Training kernel matrix\")\n",
    "axs[1].imshow(np.asmatrix(matrix_test), interpolation=\"nearest\", origin=\"upper\", cmap=\"Reds\")\n",
    "axs[1].set_title(\"Testing kernel matrix\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7152d81b",
   "metadata": {},
   "source": [
    "### Exercise 1:\n",
    "\n",
    "Often it is useful to apply Principal Component Analysis (PCA) to reduce the amount of features for algorithms. This technique trades possibly weaker accuracy for faster computation and probably easier comprehension."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e3dc241",
   "metadata": {},
   "source": [
    "Use PCA to reduce the amount of features of the iris data to 2. Plot the reduced data points. Perform QSVC and SVC on the reduced data. Compare the two scores with each other and with the non-reduced QSVC and SVC scores. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f7120bf",
   "metadata": {},
   "source": [
    "Useful methods are __PCA__ and __fit_transform__."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9a9f9b96",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "X_PCA = # PCA CODE\n",
    "\n",
    "plt.clf()\n",
    "plt.rcParams[\"figure.figsize\"] = (6, 6)\n",
    "sns.scatterplot(x=X_PCA[:, 0], y=X_PCA[:, 1], hue=y, palette=\"tab10\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45c9b7a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# QSVC CODE"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62e8175c",
   "metadata": {},
   "source": [
    "# Clustering"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc7561b6",
   "metadata": {},
   "source": [
    "An example of unsupervised learning classification that can make use of quantum techniques is Clustering. Spectral clustering particularly groups data points into $n$ many 'clusters' by minimizing the sum-of-squares within each cluster. Compared to SVC, qiskit does not have a quantum analogous implementation, but evaluated quantum kernels can still be used on the classical algorithms. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f74dd132",
   "metadata": {},
   "source": [
    "### Exercise 2:\n",
    "Create a quantum kernel, evaluate it, and apply it to a SpectralClustering classifier to get a clustering score. Also implement a SpectralClustering classifier with the default kernel and get a clustering score. Compare and describe the two scores."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7da90c61",
   "metadata": {},
   "source": [
    "Useful methods are __fit\\_predict__ and __normalized\\_mutual\\_info\\_score__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8802877",
   "metadata": {},
   "outputs": [],
   "source": [
    "X, y = make_blobs(n_samples=200, cluster_std=[1.5, 2.0, 0.5], random_state=100)\n",
    "X = MinMaxScaler().fit_transform(X)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "07e88675",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "plt.clf()\n",
    "plt.rcParams[\"figure.figsize\"] = (6, 6)\n",
    "sns.scatterplot(x=X[:, 0], y=X[:, 1], hue=y, palette=\"tab10\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c05797b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Kernel and matrix CODE"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4dad696",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Clustering CODE"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7c7d7ed",
   "metadata": {},
   "source": [
    "## Variational Quantum Classifier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eeefd683",
   "metadata": {},
   "outputs": [],
   "source": [
    "X = iris.data\n",
    "y = iris.target\n",
    "\n",
    "X = MinMaxScaler().fit_transform(X)\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=123)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e549602",
   "metadata": {},
   "source": [
    "Now we would like to classify the data using a quantum algorithm. One of the simplest classifiers is the __Variational Quantum Classifier (VQC)__. We will need to define a few new concepts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6abc92af",
   "metadata": {},
   "outputs": [],
   "source": [
    "X_map = ZZFeatureMap(feature_dimension=X.shape[1], reps=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18f618aa",
   "metadata": {},
   "source": [
    "We need to apply an __ansatz__ (classifier), which is another parameterized quantum circuit with the intention that optimizing its parameters minimizes the cost function. More repetitions lead to more trainable parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c7908f2",
   "metadata": {},
   "outputs": [],
   "source": [
    "ansatz = RealAmplitudes(num_qubits=X.shape[1], reps=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7bd886da",
   "metadata": {},
   "source": [
    "We require an __optimizer__ for the training process. Notable (local) optimizers available in qiskit, amongst others, are ADAM, SPSA and COBYLA. We do require a Sampler primitive again, but with ommission of its declaration, the VQC will later define a simple Sampler anyway."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1905f29e",
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = COBYLA(maxiter=100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37bd028e",
   "metadata": {},
   "source": [
    "Lastly, we require a __callback function__ in order to plot the progression of the algorithm as it gets closer to its optimum (evaluation of the cost function)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c99378ba",
   "metadata": {},
   "outputs": [],
   "source": [
    "vals = []     # clear cost history\n",
    "plt.rcParams[\"figure.figsize\"] = (12, 6)\n",
    "\n",
    "# VQC calls for each evaluation of objective function\n",
    "# 2 param: weights and objective function at weights\n",
    "def callback_graph(weights, obj_eval):\n",
    "    clear_output(wait=True)\n",
    "    vals.append(obj_eval)\n",
    "    plt.title(\"Cost against iteration\")\n",
    "    plt.xlabel(\"Iteration\")\n",
    "    plt.ylabel(\"Cost\")\n",
    "    plt.plot(range(len(vals)), vals)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0b07c9e",
   "metadata": {},
   "source": [
    "We can finally implement the VQC now, we have defined all the necessary tools."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d1e7236",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Initialize VQC\n",
    "vqc = VQC(feature_map=X_map, ansatz=ansatz, optimizer=optimizer, \n",
    "          callback=callback_graph)\n",
    "\n",
    "vals = []\n",
    "start = time.time()\n",
    "vqc.fit(X_train, y_train)      # Fit the training set to VQC\n",
    "elapsed = time.time() - start\n",
    "\n",
    "print(f\"Training time: {round(elapsed)} seconds\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "304d6de4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Test performance of quantum model\n",
    "train_score_quantum = vqc.score(X_train, y_train)\n",
    "test_score_quantum = vqc.score(X_test, y_test)\n",
    "print(f\"VQC on the training dataset: {train_score_quantum:.2f}\")\n",
    "print(f\"VQC on the test dataset:     {test_score_quantum:.2f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90d30d15",
   "metadata": {},
   "source": [
    "These results might be lower than that of the SVC, but they are still high enough for predicting labels reasonably accurately. Looking at the graph, we can see how the optimizer minimized the cost function value generally iteration after iteration, and how after 100 iterations, the cost stabilized. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83fa2669",
   "metadata": {},
   "source": [
    "## Benchmarking"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8af09199",
   "metadata": {},
   "source": [
    "### Exercise 3:\n",
    "\n",
    "Create a benchmark for the PCA-reduced VQC with the following considerations: <br>\n",
    "Ansatz: RealAmplitudes, EfficientSU2 <br>\n",
    "Ansatz repetitions: 1, 3 <br>\n",
    "Optimizers: COBYLA, SPSA <br>\n",
    "Optimizer max iterations: 40, 80  <br>\n",
    "Compare the scores and name which one performed the best <br>\n",
    "(First make an empty folder called _vqc_graphs_ to output into)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "03c4edd9",
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_name(obj):\n",
    "    for name, o in globals().items():\n",
    "        if o is obj:\n",
    "            return name\n",
    "\n",
    "# Ansatzs and Optimizers CODE\n",
    "RA1 = RealAmplitudes(num_qubits=X.shape[1], reps=1)\n",
    "# ...\n",
    "cobyla40 = COBYLA(maxiter=40)\n",
    "# ...\n",
    "\n",
    "ansatzs = (RA1, RA3, ESU1, ESU3)\n",
    "optimizers = (cobyla40, cobyla80, spsa40, spsa80)\n",
    "benches = [(a, b, [], f\"{print_name(a)}_{print_name(b)}\", 0) for a in ansatzs for b in optimizers]\n",
    "\n",
    "# PCA, feature map, train/test split\n",
    "\n",
    "def benchmark(ansatz, optimizer):\n",
    "    def callback(weights, cost):\n",
    "        clear_output(wait=True)\n",
    "        elapsed = time.time() - timer\n",
    "        values.append(cost)\n",
    "        plt.title(f\"{name} with cost {cost:.4f} and elapsed time {elapsed:.2f}sec.\")\n",
    "        plt.xlabel(\"Iteration\")\n",
    "        plt.ylabel(\"Cost\")\n",
    "        plt.plot(range(len(values)), values)\n",
    "        plt.savefig(f\"vqc_graphs/{name}_cost.png\") \n",
    "        plt.show()\n",
    "    \n",
    "    \n",
    "    # VQC CODE\n",
    "    \n",
    "\n",
    "    sfile = open('vqc_graphs/Scores.txt', 'a')\n",
    "    scores = f\"Name: {name}, Train_score: {train_vqc_bench:.4f}, Test_score: {test_vqc_bench:.4f}\\n\"\n",
    "    sfile.write(scores)\n",
    "    sfile.close()\n",
    "    \n",
    "with Pool(processes=None) as pool:\n",
    "    pool.starmap(benchmark, benches)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acb512c1",
   "metadata": {},
   "source": [
    "This should run for about 8min. or so. The long processes are the ones with spsa80 (if you'd like to save some time)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7058dff0",
   "metadata": {},
   "source": [
    "What did you find? Which one performed best? How does the elapsed time, scores and cost relate to each other?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eff92b3d",
   "metadata": {},
   "source": [
    "## Additional exercise!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "983d6c2f",
   "metadata": {},
   "source": [
    "### Exercise 4:\n",
    "\n",
    "Since real quantum processors in the current NISQ era are prone to decoherence, circuits such as feature maps and ansatzes are often restricted to low repetitions in order to get meaningful results. To illustrate this, we can run jobs with a noiseless simulator, _AerSimulator_, and noisy simulator, _FakeBoeblingen_, which has decoherence settings similar to what is expected at the real Boeblingen IBM QPU."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10cffe84",
   "metadata": {},
   "source": [
    "Consider the given quantum circuit below. Change the quantity of repetitions of the feature map and ansatz; for example (1,1) (1,3) (1,5) (3,1) (3,3) (5,1) (5,3) (5,5). Run these repetition pairs for the circuit with 10000 shots each, and compare the measurements in a graph. Can you see how the decoherence enters the system?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43178491",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_qubits = 4\n",
    "reps_fm = 1\n",
    "reps_az = 1\n",
    "\n",
    "circuit_fm = ZZFeatureMap(feature_dimension=n_qubits, reps=reps_fm)\n",
    "params_fm = np.arange(0,n_qubits,1) * (np.pi / n_qubits)\n",
    "binded_fm = circuit_fm.assign_parameters(params_fm)\n",
    "\n",
    "circuit_az = EfficientSU2(num_qubits=n_qubits, reps=reps_az)\n",
    "params_az = np.random.uniform(0, 2 * np.pi, n_qubits * (2 * reps_az + 2))\n",
    "binded_az = circuit_az.assign_parameters(params_az)\n",
    "\n",
    "binded_circuit = binded_fm.compose(binded_az)\n",
    "binded_circuit.measure_all()\n",
    "binded_circuit.decompose().draw(output='mpl')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36f4c486",
   "metadata": {},
   "outputs": [],
   "source": [
    "# NOISELESS SIMULATOR\n",
    "exact_backend = AerSimulator(method='statevector')\n",
    "transpiled_exact_circuit = transpile(binded_circuit, exact_backend)\n",
    "exact_job = exact_backend.run(transpiled_exact_circuit, shots=10000)\n",
    "exact_counts = exact_job.result().get_counts()\n",
    "plot_histogram(exact_counts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20898d01",
   "metadata": {},
   "outputs": [],
   "source": [
    "# NOISY SIMULATOR CODE"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17a74baf",
   "metadata": {},
   "source": [
    "### Exercise 5:\n",
    "\n",
    "Create your own! Recall the fidelity kernel that was defined for QSVC above. It did a good job, but it could be better maybe. Especially if the score was low would we need a technique to improve the kernel. Quantum Kernel Alignment (QKA) does the trick to maximise the margin using a parameterized quantum kernel. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54315efa",
   "metadata": {},
   "source": [
    "Using __QuantumKernelTrainer__ and __TrainableFidelityQuantumKernel__, train a quantum kernel and fit it to the data. See if you can find a score better than that of the QSVC above. Visualise the kernel matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb57bfc3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# QKA CODE"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "qiskitenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.19"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
