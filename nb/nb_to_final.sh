set -x
for nb in 'Section 1 - Qubit Systems' 'Section 1 - Quantum Circuits' 'Section 1 - Real Quantum';
do
  jupyter nbconvert --to notebook "${nb}.ipynb" --execute
  mv "${nb}.nbconvert.ipynb" "${nb}-solution.ipynb"
  jupyter nbconvert --to notebook "${nb}.ipynb" --execute --TagRemovePreprocessor.enabled=True --TagRemovePreprocessor.remove_cell_tags solution
  mv "${nb}.nbconvert.ipynb" "../${nb}.ipynb"
done
cp *.qasm ..
