# ISC 2024 QML Tutorial 

## Abstract
Gate-based quantum computers and their capabilities are an area that growing interest and resources are being allocated to all over the world. In particular, the possibility of exponential improvements in classical machine learning algorithms when executed by an equivalent algorithm on a quantum computer. In this ISC session, we present an introductory tutorial to quantum computing aimed at computer and data scientists and HPC users of any science domain. Our focus is to show how quantum computers are programmed today while providing the essential math and physics background required to develop quantum algorithms. After introducing some basic examples, the tutorial covers hybrid quantum classical approaches, which promise the most near-term value, focusing on familiar NP-hard graph problems as examples. Finally, we illustrate some quantum classifiers, such as the Variational Quantum Classifier, as an introduction to Quantum Machine Learning. The tutorials will use the Qiskit framework, including the newly introduced Qiskit primitives, to show how increasingly complex quantum algorithms can be constructed from available building blocks.

## Target Audience
This tutorial is aimed at computer and data scientists of any degree of skill with an interest in how quantum computers are programmed. HPC users interested in utilizing new approaches would find the integration of classical and quantum computation helpful. Young programmers are introduced to an emerging market.

## Agenda
### Section 1: Quantum Computing Basics (4h15m):
* Introduction and basic concepts
* Exercises on qubit systems
* Implementation of quantum circuits 
* Exercises on quantum circuits
* Practical considerations on real quantum computers
* Exercises on aspects of real quantum computers

### Section 2: Quantum Machine Learning (2h45m):
* Background regarding classifiers
* Quantum implementation of classical classifiers
* Exercises on implementing quantum kernels
* Quantum-designed classifiers
* Exercises on benchmarking quantum configurations
* Further curiosity into training quantum kernels

## Presenters
* Christian Boehme - christian.boehme@gwdg.de
* Lourens van Niekerk - lourens.van-niekerk@gwdg.de
* Tino Meisel - tino.meisel@gwdg.de
* Robert Schade - robert.schade@uni-paderborn.de
* Xin Wu - xin.wu@uni-paderborn.de

<!--- 
* Patrick Gelß - gelss@zib.de
* Zarin Shakibaei - shakibaei@zib.de
--->

## Institutes
* GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen - hpc@gwdg.de
* PC2 - Paderborn Center for Parallel Computing - https://pc2.uni-paderborn.de
<!--- * ZIB - Zuse Institute Berlin - info@zib.de --->

## Prerequisites
The exercises will be executed on the attendee’s laptop. All attendees will require a laptop on which python language version 3.7 or later is installed. If a suitable Python version is installed, all other requirements can easily be installed during the tutorial, provided the attendees have sufficient permissions.

Attendees will have 2 options for accessing and utilising the materials: 
1. Temporary accounts are available for use that have been set up already on the JupyterHub of the NHR Emmy Cluster.

2. A self-installation script is provided in order to run the notebooks locally.

<!---3. A guide is provided on how to run the notebooks from within VirtualBox. VirtualBox can be installed at [virtualbox.org](https://www.virtualbox.org/ "VirtualBox Homepage"). This can be done with any of the common OS's.--->


## Accessing resources options:

To prepare for the exercises, as mentioned in 'Prerequisites' above, please make use of ONE of the below options for accessing the resources.

### 1) Jupyter-HPC Option
Each attendant has been assigned a temporary account for use on the Jupyter-HPC platform of the GWDG Scientific Compute Cluster (SCC). The accounts are valid for the month of May, so further testing can be done after the conference. (Example: username- gkrs5770 ; password- c4gI+aQ!-v)

Steps to usage:
1. Go to https://jupyter.hpc.gwdg.de/hub/login .
2. Enter your assigned Username and Password in the Academic ID login.
3. In the Server Options, at 'Select a job profile:', choose 'ISC 2024: Quantum Machine Learning' and change 'Set the number of cores' to 4. Click 'Start'. Your screen should look as in the image below. ![jhspawner](/images/JupyterHub/jupyterHub_spawner.png "JupyterHub Spawner Page")
4. Once your server has been spawned and you are in the JupyterHub, open a Terminal and type the following commands
    ```
    ~$ git clone https://gitlab.gwdg.de/ag-compute-public/isc-2024-qml-tutorial.git
    ```
    Your screen should now look as in the image below. ![jhterminal](/images/JupyterHub/jupyterHub_terminal.png "JupyterHub Terminal Page")

5. Now you can enter the new folder and follow along with the workshop. Enjoy!

### 2) Local Installation

(Tested on a fresh Windows 10 and Ubuntu 22 Installation.)

1. Git is required for accessing the resources. In case Git isn't already available on your Computer: [Get Git](https://git-scm.com/downloads)

2. Get Anaconda (recommended) or pyflow
    - installation instructions for your OS can be found here: [Get Anaconda](https://www.anaconda.com/download/)
    - for Ubuntu/Debian run following after installation:
    ```
    ~$ source ~/.bashrc
    ```

3. Create a virtual Python environment 
    - if you are using a Unix or Linux OS, use the `create_conda_env.sh`
    - else: Open cmd shell or open the Anaconda Navigator and open Powershell Prompt:

    ![conda1](images/Anaconda/Anaconda-powershell.png)

    - Create a new Python 3.12 environment
    ```
    (base) ~$ conda create --name qiskitenv python=3.12
    ```
    
    - Activate the environment
    ```
    (base) ~$ conda activate qiskitenv
    ```

    - Install required packages
    ```
    (qiskitenv) ~$ conda install notebook ipykernel 
    ```
    - Create kernel
    ```
    (qiskitenv) ~$ python -m ipykernel install --user --name qiskitenv
    ```
    - Install required qiskit packages
    ```
    (qiskitenv) ~$ pip install qiskit[visualization] qiskit-machine-learning qiskit-algorithms qiskit-ibm-runtime qiskit_aer 
    ```

4. Open the notebook
    - either use VScode and select the Kernel `qiskitenv`
    - or start a jupyter instance on your machine using your Web Browser: [Jupyter Notebook Documentation](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html)
    - on Ubuntu/Debian type:
    ```
    Username@Hostname: ~$ jupyter notebook
    ```
    - on Windows use the Anaconda Navigator to start a jupyter notebook

5. Clone the repository
    - Use the below command and start enjoying the exercises.
    ```
    git clone https://gitlab.gwdg.de/ag-compute-public/isc-2024-qml-tutorial.git
    ```




<!---
### 3) Using a Virtual Machine (VM)

#### Using the VM

Install Oracle VM VirtualBox Manager for your system from https://www.virtualbox.org/wiki/Downloads . Download the VM files from https://owncloud.gwdg.de/index.php/s/X34sZ90qmSAoF1U (there should be a .vbox configuration file and a .vdi disk drive file compressed as a .zip file). Make sure the .vbox and .vdi files are together in the same folder. Open the VM Manager, then add a machine with the Add button or Machine -> Add. Navigate to the location of the .vbox file, and select it. To start the machine, just double click on it.

![vm01](/images/VM/vm-01-add_vm.png "Add a VM to VirtualBox")

Some terminology going forward: "Host" refers to your own computer/OS, where VirtualBox is installed. "Guest" refers to the VM we are using.

The VM starts with 2 processors and 4 GB of RAM allocated. If you want to increase or reduce this allocation, see the section "Expanding or Reducing VM resources".

The more useful applications have been gathered together on the desktop (file is the file explorer):

![vm02](/images/VM/vm-02-apps.png "Main available applications")

To start the Jupyter server, open the console by double clicking on its icon, then "jupyter-notebook --allow-root". This should also automatically open the server on Firefox. If it doesn't, open Firefox or another browser, and input "localhost:8888" as a URL.

![vm03](/images/VM/vm-03-jupyter.png "Console and Jupyter Server")

To terminate the server, go to the console and press Ctrl+c on your keyboard (or just close the terminal or power off the VM).

#### Changing VM resolutions

The VM should be configured to adapt the guest view port automatically if you resize the VM's window. This can lead to some graphical bugs on the guest, which are easily solved by restarting its graphical server (see section Graphical Bugs below).

If the automatic window resizing does not work, you can change the resolution from inside the guest OS, by going to Menu on the bottom left -> Setup -> Screen/Graphics Wizard -> First option (Screen Resolution/Color Depth)

![vm04](/images/VM/vm-04-resolution1.png "Resolution menu 1")
![vm05](/images/VM/vm-05-resolution2.png "Resolution menu 2")

If you experience problems with the window size adjusting on its own or not adjusting to your changes, you can change these options using the menu at the top when the VM is running. Under the View option, select Auto-Adjust Guest Display to enable or disable the automatic resizing.

#### Graphical bugs

If you notice some graphical bugs on the VM (particularly after resizing the window), click on the start menu on the bottom left, Exit, then "Restart graphical server". 

![vm06](/images/VM/vm-06-graphicalrestart.png "Restarting the graphical server")

#### The "Desktop"

The "Desktop" in this Linux version doesn't function like a normal Ubuntu/Windows Desktop, so don't expect files to show up there if you download them to a Desktop folder. It is recommended to download files to the "Downloads" folder or similar to make them easier to find.

#### Expanding or Reducing VM resources

If you find that the VM is too slow and your laptop/computer has a lot of resources (or not enough resources to run the current setup), you can expand the RAM, VRAM and CPU cores dedicated to the VM. For this purpose, stop the machine, right click on it, Settings, and change the allocated RAM (System -> Motherboard -> Base Memory), number of processors (System -> Processor -> Processors), or VRAM (Display -> Screen -> Video Memory).

![vm07](/images/VM/vm-07-ram.png "Changing RAM allocation")
![vm08](/images/VM/vm-08-cpu.png "Changing CPU allocation")
![vm09](/images/VM/vm-09-vram.png "Changing VRAM allocation")

#### Shared Clipboard

By default, the clipboard sharing between host and guest is disabled. To reenable it, either right click on the machine -> Settings -> General -> Advanced Tab -> Shared Clipboard; or while the machine is running, in the menus at the top select Devices -> Shared Clipboard. You can set the clipboard to be uni- or bi-directional.

#### Shared folder with Host

If you want to easily transfer files in or out of the machine (for example, to copy out your solved notebooks), follow these steps:

On Virtualbox:
With the machine stopped, right click on it, Settings -> Shared Folders -> Add. Select a path on your host machine, leave other options as default or empty.

On the Linux Host:
1. Open /etc/rc.d/rc.local with console (vi) or text editor
2. Modify the last command () by removing the # and changing both instances of "shared-folder" to the name you gave the shared folder in VirtualBox
3. Create the folder on the mount directory if it doesn't already exist: sudo mkdir /mnt/shared-folder (or the name you used in the previous point)
4. Restart the VM
5. Your folder will be mounted to /mnt/shared-folder in the guest, and wherever you told VirtualBox on the host. Files added here will be accesible from both machines.

If the folder does not appear, make sure it is actually shared in the VirtualBox menus.

#### Deleting/Cleaning up the VM

Before deleting the machine, right click on it and choose the option "Show in file manager". This will lead you to the location of any extra files theVirtualBox might have created for the VM. Make a note of the location. 

To delete the machine, right click on it and then Remove. You will be given the option to also delete any lingering files. To confirm all files have been cleaned, check the location mentioned in the previous paragraph, as well as the location where the .vbox and .vdi files were initially downloaded.
--->
