#!/bin/sh

conda create --name qiskitenv python=3.12
conda activate qiskitenv
conda install notebook ipykernel
python -m ipykernel install --user --name qiskitenv --display-name "QiskitEnv"
pip install pandas qiskit[visualization] qiskit-machine-learning pydantic qiskit-ibm-runtime qiskit_aer qiskit-algorithms
